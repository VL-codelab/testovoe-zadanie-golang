// Code generated by go-swagger; DO NOT EDIT.

package restapi

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"
)

var (
	// SwaggerJSON embedded version of the swagger document used at generation time
	SwaggerJSON json.RawMessage
	// FlatSwaggerJSON embedded flattened version of the swagger document used at generation time
	FlatSwaggerJSON json.RawMessage
)

func init() {
	SwaggerJSON = json.RawMessage([]byte(`{
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "Определить swagger схему в формате yaml где должны присутствовать 4 метода:\nа) метод авторизации, который принимает валидный e-mail и пароль, который должен состоять только из английских букв, цифр, при этом должна быть 1 заглавная буква, 1 маленькая и 1 цифра обязательно. После авторизации устанавливается http заголовок, с помощью которого авторизуются другие методы. Заголовок должен быть уникальным для каждого пользователя\nb) метод добавления товара\nc) метод удаления товара\nd) метод обновления товара\n\nМетоды операций с товаром не должны пропускать запросы без заголовка авторизации и возвращать http код 401 если заголовка нет или заголовок не соответствует авторизованному пользователю",
    "title": "Тестовое задание МТС",
    "contact": {
      "email": "valery.loshkarev@yandex.ru"
    },
    "version": "1.0.0"
  },
  "host": "petstore.swagger.io",
  "basePath": "/v2",
  "paths": {
    "/auth/login": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "auth"
        ],
        "summary": "Авторизация пользователя",
        "operationId": "authLogin",
        "parameters": [
          {
            "description": "Данные для авторизации",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Auth"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешная авторизация",
            "schema": {
              "$ref": "#/definitions/XSessionToken"
            }
          },
          "400": {
            "description": "Ошибка входных данных",
            "schema": {
              "$ref": "#/definitions/ValidationError"
            }
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    },
    "/goods": {
      "post": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Добавление нового товара",
        "operationId": "addGoods",
        "parameters": [
          {
            "description": "Параметры товара",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное добавление нового товара",
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    },
    "/goods/{goodsID}": {
      "put": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Обновление товара",
        "operationId": "updateGoods",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "description": "ID товара",
            "name": "goodsID",
            "in": "path",
            "required": true
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное обновление товара",
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "404": {
            "description": "Товар не найден"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      },
      "delete": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Удаление товара",
        "operationId": "deleteGoods",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "description": "ID товара",
            "name": "goodsID",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное удаление товара",
            "schema": {
              "$ref": "#/definitions/Result"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "404": {
            "description": "Товар не найден"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Auth": {
      "type": "object",
      "required": [
        "email",
        "password"
      ],
      "properties": {
        "email": {
          "description": "Email",
          "type": "string",
          "format": "email",
          "example": "example@example.com"
        },
        "password": {
          "description": "Пароль (должен содержать в себе как минимум одну цифру, строчную и заглавную буквы)",
          "type": "string"
        }
      }
    },
    "Goods": {
      "type": "object",
      "required": [
        "title",
        "cost",
        "quantity"
      ],
      "properties": {
        "cost": {
          "description": "Стоимость товара в копейках",
          "type": "integer",
          "format": "int64",
          "example": 25000
        },
        "id": {
          "type": "integer",
          "format": "int64",
          "readOnly": true,
          "example": 12
        },
        "quantity": {
          "description": "Количество товара (Например 2.5)",
          "type": "number",
          "format": "float",
          "example": 10.5
        },
        "title": {
          "description": "Название товара",
          "type": "string",
          "example": "Яблоки"
        }
      }
    },
    "InternalError": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int8"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "Result": {
      "type": "object",
      "properties": {
        "result": {
          "description": "Результат выполнения запроса",
          "type": "boolean"
        }
      }
    },
    "ValidationError": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int8"
        },
        "errors": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "field": {
                "type": "string"
              },
              "message": {
                "type": "string"
              }
            }
          }
        },
        "message": {
          "type": "string"
        }
      }
    },
    "XSessionToken": {
      "type": "object",
      "properties": {
        "token": {
          "type": "string"
        }
      }
    }
  },
  "securityDefinitions": {
    "x_session_token": {
      "type": "apiKey",
      "name": "X-Session-Token",
      "in": "header"
    }
  },
  "tags": [
    {
      "description": "Авторизация пользователя",
      "name": "auth"
    },
    {
      "description": "Добавление, обновление и удаление товара",
      "name": "goods"
    }
  ]
}`))
	FlatSwaggerJSON = json.RawMessage([]byte(`{
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "Определить swagger схему в формате yaml где должны присутствовать 4 метода:\nа) метод авторизации, который принимает валидный e-mail и пароль, который должен состоять только из английских букв, цифр, при этом должна быть 1 заглавная буква, 1 маленькая и 1 цифра обязательно. После авторизации устанавливается http заголовок, с помощью которого авторизуются другие методы. Заголовок должен быть уникальным для каждого пользователя\nb) метод добавления товара\nc) метод удаления товара\nd) метод обновления товара\n\nМетоды операций с товаром не должны пропускать запросы без заголовка авторизации и возвращать http код 401 если заголовка нет или заголовок не соответствует авторизованному пользователю",
    "title": "Тестовое задание МТС",
    "contact": {
      "email": "valery.loshkarev@yandex.ru"
    },
    "version": "1.0.0"
  },
  "host": "petstore.swagger.io",
  "basePath": "/v2",
  "paths": {
    "/auth/login": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "auth"
        ],
        "summary": "Авторизация пользователя",
        "operationId": "authLogin",
        "parameters": [
          {
            "description": "Данные для авторизации",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Auth"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешная авторизация",
            "schema": {
              "$ref": "#/definitions/XSessionToken"
            }
          },
          "400": {
            "description": "Ошибка входных данных",
            "schema": {
              "$ref": "#/definitions/ValidationError"
            }
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    },
    "/goods": {
      "post": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Добавление нового товара",
        "operationId": "addGoods",
        "parameters": [
          {
            "description": "Параметры товара",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное добавление нового товара",
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    },
    "/goods/{goodsID}": {
      "put": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Обновление товара",
        "operationId": "updateGoods",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "description": "ID товара",
            "name": "goodsID",
            "in": "path",
            "required": true
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное обновление товара",
            "schema": {
              "$ref": "#/definitions/Goods"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "404": {
            "description": "Товар не найден"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      },
      "delete": {
        "security": [
          {
            "x_session_token": []
          }
        ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "goods"
        ],
        "summary": "Удаление товара",
        "operationId": "deleteGoods",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "description": "ID товара",
            "name": "goodsID",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Успешное удаление товара",
            "schema": {
              "$ref": "#/definitions/Result"
            }
          },
          "401": {
            "description": "Отсутствует или некорректный x-session-token"
          },
          "404": {
            "description": "Товар не найден"
          },
          "405": {
            "description": "Метод не разрешен"
          },
          "500": {
            "description": "Внутренняя ошибка",
            "schema": {
              "$ref": "#/definitions/InternalError"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Auth": {
      "type": "object",
      "required": [
        "email",
        "password"
      ],
      "properties": {
        "email": {
          "description": "Email",
          "type": "string",
          "format": "email",
          "example": "example@example.com"
        },
        "password": {
          "description": "Пароль (должен содержать в себе как минимум одну цифру, строчную и заглавную буквы)",
          "type": "string"
        }
      }
    },
    "Goods": {
      "type": "object",
      "required": [
        "title",
        "cost",
        "quantity"
      ],
      "properties": {
        "cost": {
          "description": "Стоимость товара в копейках",
          "type": "integer",
          "format": "int64",
          "example": 25000
        },
        "id": {
          "type": "integer",
          "format": "int64",
          "readOnly": true,
          "example": 12
        },
        "quantity": {
          "description": "Количество товара (Например 2.5)",
          "type": "number",
          "format": "float",
          "example": 10.5
        },
        "title": {
          "description": "Название товара",
          "type": "string",
          "example": "Яблоки"
        }
      }
    },
    "InternalError": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int8"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "Result": {
      "type": "object",
      "properties": {
        "result": {
          "description": "Результат выполнения запроса",
          "type": "boolean"
        }
      }
    },
    "ValidationError": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int8"
        },
        "errors": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "field": {
                "type": "string"
              },
              "message": {
                "type": "string"
              }
            }
          }
        },
        "message": {
          "type": "string"
        }
      }
    },
    "XSessionToken": {
      "type": "object",
      "properties": {
        "token": {
          "type": "string"
        }
      }
    }
  },
  "securityDefinitions": {
    "x_session_token": {
      "type": "apiKey",
      "name": "X-Session-Token",
      "in": "header"
    }
  },
  "tags": [
    {
      "description": "Авторизация пользователя",
      "name": "auth"
    },
    {
      "description": "Добавление, обновление и удаление товара",
      "name": "goods"
    }
  ]
}`))
}
