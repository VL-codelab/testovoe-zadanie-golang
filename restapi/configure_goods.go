// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"mts/restapi/operations"
	"mts/restapi/operations/auth"
	"mts/restapi/operations/goods"
	"mts/app"
	"github.com/go-redis/redis"
	"time"
	"crypto/tls"
	"github.com/go-openapi/swag"
	"fmt"
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

type ConfigFile struct {
	Path string `long:"config"`
}

var configFile = ConfigFile{}

func configureFlags(api *operations.GoodsAPI) {
	api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{
		swag.CommandLineOptionsGroup{
			ShortDescription: "Launch options",
			Options: &configFile,
		},
	}
}

func configureAPI(api *operations.GoodsAPI) http.Handler {
	api.ServeError = errors.ServeError

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	config := readConfigFromFile()

	redisClient := newRedisClient(config)
	instance := app.Instance(redisClient)

	api.XSessionTokenAuth = instance.CheckAuthHandler

	api.AuthAuthLoginHandler = auth.AuthLoginHandlerFunc(instance.AuthLoginAction)
	api.GoodsAddGoodsHandler = goods.AddGoodsHandlerFunc(instance.AddGoodsAction)
	api.GoodsDeleteGoodsHandler = goods.DeleteGoodsHandlerFunc(instance.DeleteGoodsAction)
	api.GoodsUpdateGoodsHandler = goods.UpdateGoodsHandlerFunc(instance.UpdateGoodsAction)

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}

type Configuration struct {
	Redis struct {
		Server string `yaml:"server"`
		IdleTimeout int64 `yaml:"idle"`
		MaxIdle int `yaml:"max_idle"`
	}
}

func readConfigFromFile() *Configuration {
	yamlFile, err := ioutil.ReadFile(configFile.Path)
	if err != nil {
		panic(fmt.Sprintf("Ошибка чтения конфигурационного файла: %s",  err.Error()))
	}
    cfg := Configuration{}
	err = yaml.Unmarshal(yamlFile, &cfg)
	if err != nil {
		panic(fmt.Sprintf("Ошибка декодирования конфигурационного файла: %s",  err.Error()))
	}
	return &cfg
}

func newRedisClient(cfg *Configuration) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:        cfg.Redis.Server,
		DB:          0,
		IdleTimeout: time.Duration(cfg.Redis.IdleTimeout) * time.Millisecond,
		PoolSize:    cfg.Redis.MaxIdle,
	})
}