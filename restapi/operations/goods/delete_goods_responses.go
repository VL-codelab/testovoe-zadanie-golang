// Code generated by go-swagger; DO NOT EDIT.

package goods

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "mts/models"
)

// DeleteGoodsOKCode is the HTTP code returned for type DeleteGoodsOK
const DeleteGoodsOKCode int = 200

/*DeleteGoodsOK Успешное удаление товара

swagger:response deleteGoodsOK
*/
type DeleteGoodsOK struct {

	/*
	  In: Body
	*/
	Payload *models.Result `json:"body,omitempty"`
}

// NewDeleteGoodsOK creates DeleteGoodsOK with default headers values
func NewDeleteGoodsOK() *DeleteGoodsOK {

	return &DeleteGoodsOK{}
}

// WithPayload adds the payload to the delete goods o k response
func (o *DeleteGoodsOK) WithPayload(payload *models.Result) *DeleteGoodsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the delete goods o k response
func (o *DeleteGoodsOK) SetPayload(payload *models.Result) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DeleteGoodsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// DeleteGoodsUnauthorizedCode is the HTTP code returned for type DeleteGoodsUnauthorized
const DeleteGoodsUnauthorizedCode int = 401

/*DeleteGoodsUnauthorized Отсутствует или некорректный x-session-token

swagger:response deleteGoodsUnauthorized
*/
type DeleteGoodsUnauthorized struct {
}

// NewDeleteGoodsUnauthorized creates DeleteGoodsUnauthorized with default headers values
func NewDeleteGoodsUnauthorized() *DeleteGoodsUnauthorized {

	return &DeleteGoodsUnauthorized{}
}

// WriteResponse to the client
func (o *DeleteGoodsUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// DeleteGoodsNotFoundCode is the HTTP code returned for type DeleteGoodsNotFound
const DeleteGoodsNotFoundCode int = 404

/*DeleteGoodsNotFound Товар не найден

swagger:response deleteGoodsNotFound
*/
type DeleteGoodsNotFound struct {
}

// NewDeleteGoodsNotFound creates DeleteGoodsNotFound with default headers values
func NewDeleteGoodsNotFound() *DeleteGoodsNotFound {

	return &DeleteGoodsNotFound{}
}

// WriteResponse to the client
func (o *DeleteGoodsNotFound) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(404)
}

// DeleteGoodsMethodNotAllowedCode is the HTTP code returned for type DeleteGoodsMethodNotAllowed
const DeleteGoodsMethodNotAllowedCode int = 405

/*DeleteGoodsMethodNotAllowed Метод не разрешен

swagger:response deleteGoodsMethodNotAllowed
*/
type DeleteGoodsMethodNotAllowed struct {
}

// NewDeleteGoodsMethodNotAllowed creates DeleteGoodsMethodNotAllowed with default headers values
func NewDeleteGoodsMethodNotAllowed() *DeleteGoodsMethodNotAllowed {

	return &DeleteGoodsMethodNotAllowed{}
}

// WriteResponse to the client
func (o *DeleteGoodsMethodNotAllowed) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(405)
}

// DeleteGoodsInternalServerErrorCode is the HTTP code returned for type DeleteGoodsInternalServerError
const DeleteGoodsInternalServerErrorCode int = 500

/*DeleteGoodsInternalServerError Внутренняя ошибка

swagger:response deleteGoodsInternalServerError
*/
type DeleteGoodsInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *models.InternalError `json:"body,omitempty"`
}

// NewDeleteGoodsInternalServerError creates DeleteGoodsInternalServerError with default headers values
func NewDeleteGoodsInternalServerError() *DeleteGoodsInternalServerError {

	return &DeleteGoodsInternalServerError{}
}

// WithPayload adds the payload to the delete goods internal server error response
func (o *DeleteGoodsInternalServerError) WithPayload(payload *models.InternalError) *DeleteGoodsInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the delete goods internal server error response
func (o *DeleteGoodsInternalServerError) SetPayload(payload *models.InternalError) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DeleteGoodsInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
