// Code generated by go-swagger; DO NOT EDIT.

package goods

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "mts/models"
)

// AddGoodsOKCode is the HTTP code returned for type AddGoodsOK
const AddGoodsOKCode int = 200

/*AddGoodsOK Успешное добавление нового товара

swagger:response addGoodsOK
*/
type AddGoodsOK struct {

	/*
	  In: Body
	*/
	Payload *models.Goods `json:"body,omitempty"`
}

// NewAddGoodsOK creates AddGoodsOK with default headers values
func NewAddGoodsOK() *AddGoodsOK {

	return &AddGoodsOK{}
}

// WithPayload adds the payload to the add goods o k response
func (o *AddGoodsOK) WithPayload(payload *models.Goods) *AddGoodsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the add goods o k response
func (o *AddGoodsOK) SetPayload(payload *models.Goods) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *AddGoodsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// AddGoodsUnauthorizedCode is the HTTP code returned for type AddGoodsUnauthorized
const AddGoodsUnauthorizedCode int = 401

/*AddGoodsUnauthorized Отсутствует или некорректный x-session-token

swagger:response addGoodsUnauthorized
*/
type AddGoodsUnauthorized struct {
}

// NewAddGoodsUnauthorized creates AddGoodsUnauthorized with default headers values
func NewAddGoodsUnauthorized() *AddGoodsUnauthorized {

	return &AddGoodsUnauthorized{}
}

// WriteResponse to the client
func (o *AddGoodsUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// AddGoodsMethodNotAllowedCode is the HTTP code returned for type AddGoodsMethodNotAllowed
const AddGoodsMethodNotAllowedCode int = 405

/*AddGoodsMethodNotAllowed Метод не разрешен

swagger:response addGoodsMethodNotAllowed
*/
type AddGoodsMethodNotAllowed struct {
}

// NewAddGoodsMethodNotAllowed creates AddGoodsMethodNotAllowed with default headers values
func NewAddGoodsMethodNotAllowed() *AddGoodsMethodNotAllowed {

	return &AddGoodsMethodNotAllowed{}
}

// WriteResponse to the client
func (o *AddGoodsMethodNotAllowed) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(405)
}

// AddGoodsInternalServerErrorCode is the HTTP code returned for type AddGoodsInternalServerError
const AddGoodsInternalServerErrorCode int = 500

/*AddGoodsInternalServerError Внутренняя ошибка

swagger:response addGoodsInternalServerError
*/
type AddGoodsInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *models.InternalError `json:"body,omitempty"`
}

// NewAddGoodsInternalServerError creates AddGoodsInternalServerError with default headers values
func NewAddGoodsInternalServerError() *AddGoodsInternalServerError {

	return &AddGoodsInternalServerError{}
}

// WithPayload adds the payload to the add goods internal server error response
func (o *AddGoodsInternalServerError) WithPayload(payload *models.InternalError) *AddGoodsInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the add goods internal server error response
func (o *AddGoodsInternalServerError) SetPayload(payload *models.InternalError) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *AddGoodsInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
