SVCNAME ?= goods-server
DESTDIR ?= ./build
CURRENT := $(shell pwd)

deps:
	go get -u github.com/golang/dep
	dep ensure
	go get -u github.com/go-swagger/go-swagger

build:
	mkdir -p $(CURRENT)/build
	go build -o ${DESTDIR}/${SVCNAME} cmd/${SVCNAME}/*.go

build-win:
	mkdir -p $(CURRENT)/build
	go build -o ${DESTDIR}/${SVCNAME}.exe cmd/${SVCNAME}/*.go

start:
	${DESTDIR}/${SVCNAME} --port 3003 --config="config.yaml"

doc:
	swagger serve ./swagger.yaml -p 3002

test:
	GOCACHE=off go test -v --race ./app/

.PHONY: build build-win deps start doc test

