package app

import (
	"mts/models"
	"mts/restapi/operations/goods"
	"fmt"
	"encoding/json"
	"github.com/go-openapi/runtime/middleware"
)

const (
	// Ключ для храненния товаров в Redis
	GoodsKey = "_goods"
	// Ключ для храненния инкремента товара в Redis
	GoodsIncr = "_goods_incr"
)

func (app *App) AddGoodsAction(params goods.AddGoodsParams, principal interface{}) middleware.Responder {
	incr, err := app.redis.Incr(GoodsIncr).Result()
	if err != nil {
		return goods.NewAddGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrInternalMsg, err.Error()),
			},
		)
	}

	serializedGoods, _ := json.Marshal(params.Body)
	if _, err := app.redis.HSet(GoodsKey, fmt.Sprintf("%v", incr), serializedGoods).Result(); err != nil{
		return goods.NewAddGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrInternalMsg, err.Error()),
			},
		)
	}

	return goods.NewAddGoodsOK().WithPayload(
		&models.Goods{
			ID: incr,
			Cost: params.Body.Cost,
			Title: params.Body.Title,
			Quantity: params.Body.Quantity,
		},
	)
}

func (app *App) UpdateGoodsAction(params goods.UpdateGoodsParams, principal interface{}) middleware.Responder {
	goodsId := fmt.Sprintf("%v", params.GoodsID)
	goodsExist, err := app.redis.HExists(GoodsKey, goodsId).Result()
	if err != nil {
		return goods.NewUpdateGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrValidationMsg, err.Error()),
			},
		)
	}

	if !goodsExist {
		return goods.NewUpdateGoodsNotFound()
	}

	newSerializedGoods, _ := json.Marshal(params.Body)
	if _, err := app.redis.HSet(GoodsKey, goodsId, newSerializedGoods).Result(); err != nil{
		return goods.NewAddGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrInternalMsg, err.Error()),
			},
		)
	}

	return goods.NewUpdateGoodsOK().WithPayload(
		&models.Goods{
			ID: params.GoodsID,
			Cost: params.Body.Cost,
			Title: params.Body.Title,
			Quantity: params.Body.Quantity,
		},
	)
}

func (app *App) DeleteGoodsAction(params goods.DeleteGoodsParams, principal interface{}) middleware.Responder {
	goodsId := fmt.Sprintf("%v", params.GoodsID)
	goodsExist, err := app.redis.HExists(GoodsKey, goodsId).Result()
	if err != nil {
		return goods.NewUpdateGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrInternalMsg, err.Error()),
			},
		)
	}

	if !goodsExist {
		return goods.NewUpdateGoodsNotFound()
	}

	_, err = app.redis.HDel(GoodsKey, goodsId).Result()
	if err != nil {
		return goods.NewUpdateGoodsInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: errMsg(ErrInternalMsg, err.Error()),
			},
		)
	}

	return goods.NewDeleteGoodsOK().WithPayload(
		&models.Result{
			Result: true,
		},
	)
}