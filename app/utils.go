package app

import (
	"unicode"
	"crypto/sha256"
	"encoding/hex"
	"time"
	"fmt"
)
// Метод для валидации пароля. 1 заглавная буква, 1 строчная, 1 цифра
func validatePassword(s string) bool {
	var number, upper, lower bool
	for _, s := range s {
		switch {
		case unicode.IsNumber(s):
			number = true
		case unicode.IsUpper(s):
			upper = true
		case unicode.IsLower(s):
			lower = true
		default:
		}
	}
	if number && upper && lower {
		return true
	}

	return false
}

// Метод для получения хеша SHA256 из строки
func GetSHA256FromString(s string) string {
	sha := sha256.New()
	sha.Write([]byte(s))

	return hex.EncodeToString(sha.Sum(nil))
}

// Метод для получения уникального ID
// UnixNano + Salt
func GenerateUniqId(salt string) string {
	t := time.Now().UnixNano()

	return GetSHA256FromString(fmt.Sprintf("%v", t) + string('_') + salt)
}
