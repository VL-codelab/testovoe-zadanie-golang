package app

import (
	"fmt"
	"github.com/go-redis/redis"
)

type App struct {
	redis *redis.Client
}

const (
	ErrValidationCode = 1
	ErrInternalCode   = 2

	ErrValidationMsg = "Ошибка валидации"
	ErrInternalMsg   = "Внутренняя ошибка"
)

// Получение экземпляра приложения
func Instance(redisClient *redis.Client) *App {
	a := &App{
		redis: redisClient,
	}

	return a
}

// Обертка для ошибок
func errMsg(msg string, args ...interface{}) string {
	return fmt.Sprintf(msg+"%s", args)
}
