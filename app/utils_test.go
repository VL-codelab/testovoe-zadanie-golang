package app

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetSHA256FromString(t *testing.T) {
	r := GetSHA256FromString("test")
	assert.Equal(t, "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08", r)
}

func TestValidatePassword(t *testing.T) {
	assert.Equal(t, validatePassword("123"), false)
	assert.Equal(t, validatePassword("hhh"), false)
	assert.Equal(t, validatePassword("h1t"), false)
	assert.Equal(t, validatePassword(""), false)
	assert.Equal(t, validatePassword("J6m"), true)
	assert.Equal(t, validatePassword("6kM"), true)
}
