package app

import (
	"mts/models"
	"mts/restapi/operations/auth"
	"encoding/json"
	"fmt"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/errors"
)

const (
	// Ключ для хранения токенов в Redis
	TokensKey = "_tokens"
)

type Identity struct {
	Email string `json:"email"`
}

func (app *App) AuthLoginAction(params auth.AuthLoginParams) middleware.Responder {
	email := *params.Body.Email
	password := *params.Body.Password
	if !validatePassword(password) {
		return auth.NewAuthLoginBadRequest().WithPayload(&models.ValidationError{
			Code: ErrValidationCode,
			Message: ErrValidationMsg,
			Errors: []*models.ValidationErrorErrorsItems0{
				&models.ValidationErrorErrorsItems0{
					Field:   "password",
					Message: "Пароль должен содержать в себе как минимум одну цифру, строчную и заглавную буквы",
				},
			},
		})
	}

	token := GenerateUniqId(fmt.Sprintf("%v", email))
	identity := Identity{
		Email: string(email),
	}

	str, _ := json.Marshal(identity)

	if err := app.redis.HSet(TokensKey, token, str).Err(); err != nil {
		return auth.NewAuthLoginInternalServerError().WithPayload(
			&models.InternalError{
				Code: ErrInternalCode,
				Message: ErrInternalMsg + " " + err.Error(),
			},
		)
	}

	return auth.NewAuthLoginOK().WithPayload(&models.XSessionToken{
		Token: token,
	})
}

func (app *App) CheckAuthHandler(token string) (interface{}, error){
	if s, err := app.redis.HGet(TokensKey, token).Bytes(); err == nil {
		identity := Identity{}
		if err = json.Unmarshal(s, &identity); err == nil {
			return identity, err
		}
	}

	return nil, errors.New(401, "Некорректный X-Session-Token")
}
